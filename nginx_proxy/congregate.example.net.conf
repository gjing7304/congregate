upstream github {
  server        example.github.url.com:443;
}

server {
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/congregate.example.net/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/congregate.example.net/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot  
    server_name         congregate.example.net;
    ignore_invalid_headers off;

  location / {
    proxy_set_header Accept-Encoding "";
    sub_filter "example.github.url.com" "congregate.example.net";
    sub_filter_once     off;
    sub_filter_types    application/json;    

    proxy_pass  https://example.github.url.com;
    proxy_ssl_trusted_certificate /etc/nginx/cert.pem;
    proxy_ssl_verify       on;
    proxy_ssl_verify_depth 2;   

    proxy_pass_request_headers on;
    proxy_set_header    X-Real-IP           $remote_addr;
    proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Proto   $scheme;
    proxy_set_header    Host                example.github.url.com;
    client_max_body_size       40960M;
  }
}
